/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dezzeddi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 15:32:14 by dezzeddi          #+#    #+#             */
/*   Updated: 2017/11/15 23:17:29 by vpupkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_header.h"

int		**create_empty_array(int width, int lines)
{
	int		i;
	int		**array;

	i = 0;
	array = (int**)malloc(sizeof(int*) * (lines + 1));
	while (i < lines)
	{
		array[i] = (int*)malloc(sizeof(int) * (width + 1));
		i++;
	}
	return (array);
}

int		min_var(int a, int b, int c)
{
	if (a <= b && a <= c)
		return (a);
	else if (b <= a && b <= c)
		return (b);
	else if (c <= a && c <= b)
		return (c);
	else
		return (0);
}

int		set_read_pointer(char *filename)
{
	int		fd;
	char	buf[1];

	fd = open(filename, O_RDONLY);
	while (read(fd, buf, 1) > 0)
		if (buf[0] == '\n')
			break ;
	return (fd);
}

int		**matrix_converter(int **array, int width, int lines)
{
	int		i;
	int		j;

	i = 0;
	while (i < lines)
	{
		j = 0;
		while (j < width)
		{
			array[i][j] = count_pos_value(array, i, j);
			j++;
		}
		i++;
	}
	return (array);
}

int		*find_bsq(int **array, int width, int lines)
{
	int		i;
	int		j;
	int		*res;

	res = malloc(sizeof(int) * 4);
	i = 0;
	res[0] = 0;
	while (i < lines)
	{
		j = 0;
		while (j < width)
		{
			if (res[0] < array[i][j])
			{
				res[0] = array[i][j];
				res[1] = j;
				res[2] = i;
			}
			j++;
		}
		i++;
	}
	res[1] = res[1] - res[0] + 1;
	res[2] = res[2] - res[0] + 1;
	return (res);
}
