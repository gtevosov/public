/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vpupkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 22:06:39 by vpupkin           #+#    #+#             */
/*   Updated: 2017/11/15 23:50:58 by vpupkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_header.h"

int		g_i;

void			closefile(int fd)
{
	close(fd);
}

char			*get_params(char *filename, int param)
{
	int		fd;
	char	buf[100];
	char	*str;
	int		j;

	g_i = 0;
	j = -1;
	fd = open(filename, O_RDONLY);
	if (read(fd, buf, 100) > 0)
		while (buf[g_i] != '\n')
			g_i++;
	else
		exit(-1);
	str = malloc(sizeof(char) * (g_i + 1));
	if (param == 1)
		while (++j < g_i - 3)
			str[j] = buf[j];
	else if (param == 2)
		while (j++ < 3)
		{
			str[j] = buf[g_i - 3];
			g_i++;
		}
	closefile(fd);
	return (str);
}

unsigned int	get_width(char *filename)
{
	int				fd;
	int				j;
	char			buf[1];
	unsigned int	i;

	i = 0;
	j = 0;
	fd = open(filename, O_RDONLY);
	while (read(fd, buf, 1) > 0)
	{
		if (buf[0] == '\n')
		{
			j++;
			if (j == 2)
				return (i - 1);
			i = 0;
		}
		i++;
	}
	if (close(fd) == -1)
		ft_putstr("close() error");
	return (0);
}

void			loop(void)
{
	char	*str;
	int		i;

	i = 0;
	str = malloc(sizeof(str) * 100);
	read(0, str, 99);
	parse_file(str);
	free(str);
}

int				main(int argc, char **argv)
{
	int		i;

	if (argc == 1)
		loop();
	else if (argc == 2)
	{
		parse_file(argv[1]);
	}
	else if (argc > 2)
	{
		i = 1;
		while (i < (argc))
		{
			parse_file(argv[i]);
			if (i == (argc - 1))
				break ;
			ft_putchar('\n');
			i++;
		}
	}
	return (0);
}
